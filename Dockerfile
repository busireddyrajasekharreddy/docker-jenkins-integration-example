FROM openjdk:11
EXPOSE 8081
ADD target/docker_jenkins_integration_example.jar docker_jenkins_integration_example.jar
ENTRYPOINT ["java","-jar","/docker_jenkins_integration_example.jar"]
